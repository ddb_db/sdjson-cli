package appruntime

type AppInfo struct {
	Version   string
	BuildDate string
}

var AppDetails AppInfo
var LogDir string
