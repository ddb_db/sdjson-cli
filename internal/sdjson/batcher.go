package sdjson

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/gammazero/workerpool"
	"github.com/google/uuid"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

func NewBatcherClient(
	using ClientWithResponsesInterface,
	schedMD5PoolSize,
	schedMD5BatchSize,
	schedPoolSize,
	schedBatchSize,
	progPoolSize,
	progBatchSize int) BatcherClient {
	return BatcherClient{
		ClientWithResponsesInterface: using,

		scheduleMD5BatchSize: schedMD5BatchSize,
		scheduleMD5PoolSize:  schedMD5PoolSize,
		scheduleBatchSize:    schedBatchSize,
		schedulePoolSize:     schedPoolSize,
		progBatchSize:        progBatchSize,
		progPoolSize:         progPoolSize,
	}
}

type BatcherClient struct {
	ClientWithResponsesInterface

	scheduleMD5BatchSize int
	scheduleMD5PoolSize  int
	scheduleBatchSize    int
	schedulePoolSize     int
	progBatchSize        int
	progPoolSize         int
}

func (clnt BatcherClient) ProgramsWithResponse(ctx context.Context, body ProgramsJSONRequestBody, reqEditors ...RequestEditorFn) (*ProgramsResponse, error) {
	batchID := uuid.New().String()
	scope := wlog.Fields{"id": batchID}
	wlog.WithScope(scope).Debugf("batcher: fetching %d programs", len(body))
	wpool := workerpool.New(clnt.progPoolSize)
	batches := lo.Chunk(body, clnt.progBatchSize)
	respChan := make(chan any, len(batches))
	doneChan := make(chan struct{})

	// Response handler
	json200Resp := make([]Program, 0, len(body))
	jsonDefaultResp := make([]*ErrorResponse, 0)
	errs := make([]error, 0)
	go func() {
		for resp := range respChan {
			switch v := resp.(type) {
			case error:
				wlog.WithScope(scope).Warning("batcher: program fetch error")
				errs = append(errs, v)
			case *ProgramsResponse:
				switch v.StatusCode() {
				case http.StatusOK:
					json200Resp = append(json200Resp, *v.JSON200...)
				default:
					wlog.WithScope(scope).Warning("batcher: program pull failed")
					jsonDefaultResp = append(jsonDefaultResp, v.JSONDefault)
				}
			}
		}
		doneChan <- struct{}{}
		wlog.WithScope(scope).Debug("batcher: program response handler done")
	}()

	for i, b := range batches {
		i := i
		b := b
		wpool.Submit(func() {
			func(s []string) {
				timer := fntimer.New()
				timer.Start()

				resp, err := clnt.ClientWithResponsesInterface.ProgramsWithResponse(ctx, s, reqEditors...)
				if err != nil {
					respChan <- err
					return
				}
				respChan <- resp
				stats := timer.Stop("")
				wlog.WithScope(wlog.Fields{"idx": i, "size": len(s), "id": batchID, "stats": stats.String()}).Debug("batcher: prog batch completed")
			}(b)
		})
	}

	wpool.StopWait()
	close(respChan)
	<-doneChan

	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}

	var json200 *[]Program
	var jsonDef *ErrorResponse
	var httpBody []byte
	var err error
	httpResp := http.Response{
		StatusCode: http.StatusOK,
		Status:     http.StatusText(http.StatusOK),
	}

	if len(jsonDefaultResp) > 0 {
		httpResp.StatusCode = http.StatusInternalServerError
		httpResp.Status = http.StatusText(httpResp.StatusCode)
		lo.Reduce(jsonDefaultResp, func(out string, in *BasicResponseWithError, idx int) string {
			return fmt.Sprintf("%s\n%s", out, in.Message)
		}, "")
		httpBody, err = json.Marshal(jsonDef)
		if err != nil {
			panic(err)
		}
	} else {
		json200 = &json200Resp
		httpBody, err = json.Marshal(json200)
		if err != nil {
			panic(err)
		}
	}

	return &ProgramsResponse{
		Body:         httpBody,
		JSON200:      json200,
		JSONDefault:  jsonDef,
		HTTPResponse: &httpResp,
	}, nil
}

func (clnt BatcherClient) SchedulesWithResponse(ctx context.Context, body SchedulesJSONRequestBody, reqEditors ...RequestEditorFn) (*SchedulesResponse, error) {
	batchID := uuid.New().String()
	scope := wlog.Fields{"id": batchID}
	wlog.WithScope(scope).Debugf("batcher: fetching %d schedules", len(body))
	wpool := workerpool.New(clnt.schedulePoolSize)
	batches := lo.Chunk(body, clnt.scheduleBatchSize)
	respChan := make(chan any, len(batches))
	doneChan := make(chan struct{})

	// Response handler
	json200Resp := make([]Schedule, 0, len(body))
	jsonDefaultResp := make([]*ErrorResponse, 0)
	errs := make([]error, 0)
	go func() {
		for resp := range respChan {
			switch v := resp.(type) {
			case error:
				wlog.WithScope(scope).Warning("batcher: schedule pull error")
				errs = append(errs, v)
			case *SchedulesResponse:
				switch v.StatusCode() {
				case http.StatusOK:
					json200Resp = append(json200Resp, *v.JSON200...)
				default:
					wlog.WithScope(scope).Warning("batcher: schedule pull failed")
					jsonDefaultResp = append(jsonDefaultResp, v.JSONDefault)
				}
			}
		}
		doneChan <- struct{}{}
		wlog.WithScope(scope).Debug("batcher: sched response handler done")
	}()

	for i, b := range batches {
		i := i
		b := b
		wpool.Submit(func() {
			func(s []StationRequest) {
				timer := fntimer.New()
				timer.Start()

				resp, err := clnt.ClientWithResponsesInterface.SchedulesWithResponse(ctx, s, reqEditors...)
				if err != nil {
					respChan <- err
					return
				}
				respChan <- resp
				stats := timer.Stop("")
				wlog.WithScope(wlog.Fields{"idx": i, "size": len(s), "id": batchID, "stats": stats.String()}).Debug("batcher: sched batch completed")
			}(b)
		})
	}

	wpool.StopWait()
	close(respChan)
	<-doneChan

	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}

	var json200 *[]Schedule
	var jsonDef *ErrorResponse
	var httpBody []byte
	var err error
	httpResp := http.Response{
		StatusCode: http.StatusOK,
		Status:     http.StatusText(http.StatusOK),
	}

	if len(jsonDefaultResp) > 0 {
		httpResp.StatusCode = http.StatusInternalServerError
		httpResp.Status = http.StatusText(httpResp.StatusCode)
		lo.Reduce(jsonDefaultResp, func(out string, in *BasicResponseWithError, idx int) string {
			return fmt.Sprintf("%s\n%s", out, in.Message)
		}, "")
		httpBody, err = json.Marshal(jsonDef)
		if err != nil {
			panic(err)
		}
	} else {
		json200 = &json200Resp
		httpBody, err = json.Marshal(json200)
		if err != nil {
			panic(err)
		}
	}

	return &SchedulesResponse{
		Body:         httpBody,
		JSON200:      json200,
		JSONDefault:  jsonDef,
		HTTPResponse: &httpResp,
	}, nil
}

func (clnt BatcherClient) ScheduleMD5WithResponse(ctx context.Context, ids []StationRequest, reqEditors ...RequestEditorFn) (*ScheduleMD5Response, error) {
	batchID := uuid.New().String()
	scope := wlog.Fields{"id": batchID}
	wlog.WithScope(scope).Debugf("batcher: fetching %d schedule md5s", len(ids))
	wpool := workerpool.New(clnt.scheduleMD5PoolSize)
	batches := lo.Chunk(ids, clnt.scheduleMD5BatchSize)
	respChan := make(chan any, len(batches))
	doneChan := make(chan struct{})

	// Response handler
	json200Resp := make(map[string]map[string]StationStatus)
	jsonDefaultResp := make([]*ErrorResponse, 0)
	errs := make([]error, 0)
	go func() {
		for resp := range respChan {
			switch v := resp.(type) {
			case error:
				wlog.WithScope(scope).Warning("batcher: station md5 error")
				errs = append(errs, v)
			case *ScheduleMD5Response:
				switch v.StatusCode() {
				case http.StatusOK:
					json200Resp = lo.Assign([]map[string]map[string]StationStatus{json200Resp, *v.JSON200}...)
				default:
					wlog.WithScope(scope).Warning("batcher: station md5 failed")
					jsonDefaultResp = append(jsonDefaultResp, v.JSONDefault)
				}
			}
		}
		doneChan <- struct{}{}
		wlog.WithScope(scope).Debug("batcher: sched md5 response handler done")
	}()

	// TODO better track the batch number, esp when it errors
	for i, b := range batches {
		i := i
		b := b
		wpool.Submit(func() {
			func(s []StationRequest) {
				timer := fntimer.New()
				timer.Start()
				resp, err := clnt.ClientWithResponsesInterface.ScheduleMD5WithResponse(ctx, s, reqEditors...)
				if err != nil {
					respChan <- err
					return
				}
				respChan <- resp
				stats := timer.Stop("")
				wlog.WithScope(wlog.Fields{"idx": i, "size": len(s), "id": batchID, "stats": stats.String()}).Debug("batcher: sched md5 batch completed")
			}(b)
		})
	}

	wpool.StopWait()
	close(respChan)
	<-doneChan

	if len(errs) > 0 {
		return nil, errors.Join(errs...)
	}

	var json200 *map[string]map[string]StationStatus
	var jsonDef *ErrorResponse
	var body []byte
	var err error
	// https://stackoverflow.com/questions/33978216/create-http-response-instance-with-sample-body-string-in-golang
	httpResp := http.Response{
		StatusCode: http.StatusOK,
		Status:     http.StatusText(http.StatusOK),
	}

	if len(jsonDefaultResp) > 0 {
		httpResp.StatusCode = http.StatusInternalServerError
		httpResp.Status = http.StatusText(httpResp.StatusCode)
		lo.Reduce(jsonDefaultResp, func(out string, in *BasicResponseWithError, idx int) string {
			return fmt.Sprintf("%s\n%s", out, in.Message)
		}, "")
		body, err = json.Marshal(jsonDef)
		if err != nil {
			panic(err)
		}
	} else {
		json200 = &json200Resp
		body, err = json.Marshal(json200)
		if err != nil {
			panic(err)
		}
	}

	return &ScheduleMD5Response{
		Body:         body,
		JSON200:      json200,
		JSONDefault:  jsonDef,
		HTTPResponse: &httpResp,
	}, nil
}
