package sdjson

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"os"
	"path/filepath"

	"github.com/google/uuid"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/appruntime"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

const traceHeader = "X-Sdjsoncli-Trace-ID"

func newSDTransport(debug bool) sdTransport {
	return sdTransport{
		debug: debug,
	}
}

type sdTransport struct {
	debug      bool
	id         string
	filePrefix string
}

func (st sdTransport) RoundTrip(r *http.Request) (*http.Response, error) {
	r.Header.Add("User-Agent", fmt.Sprintf("sdjsoncli/%s", appruntime.AppDetails.Version))

	if st.debug {
		st.id = uuid.New().String()
		r.Header.Add(traceHeader, st.id)
		st.filePrefix = filepath.Clean(fmt.Sprintf("%s/%s", appruntime.LogDir, st.id))

		bytes, _ := httputil.DumpRequestOut(r, true)
		go func() {
			os.WriteFile(st.filePrefix+".req", bytes, 0600)
		}()
	}

	t := fntimer.New()
	t.Start()
	resp, err := http.DefaultTransport.RoundTrip(r)
	// err is returned after dumping the response
	t.Stop("http:req:" + st.id)

	if st.debug && resp != nil {
		resp.Header.Add(traceHeader, st.id)
		bytes, _ := httputil.DumpResponse(resp, true)
		go func() {
			os.WriteFile(st.filePrefix+".resp", bytes, 0600)
		}()
		wlog.WithScope(wlog.Fields{"url": r.URL.String(), "id": st.id}).Debug("http req")
	}

	return resp, err
}
