package sdjson

import (
	"context"
	"fmt"
	"net/http"

	"github.com/deepmap/oapi-codegen/pkg/securityprovider"
	"github.com/vargspjut/wlog"
)

func Login(server, username, password string, debug bool) *ClientWithResponses {
	clntOpts := make([]ClientOption, 0, 1)
	httpClnt := &http.Client{
		Transport: newSDTransport(debug && wlog.DefaultLogger().GetLogLevel() == wlog.Dbg),
	}
	clntOpts = append(clntOpts, WithHTTPClient(httpClnt))

	clnt, err := NewClientWithResponses(server, clntOpts...)
	if err != nil {
		panic(err)
	}

	resp, err := clnt.LoginWithResponse(context.TODO(), LoginJSONRequestBody{
		Username: username,
		Password: password,
	})
	if err != nil {
		panic(err)
	}

	switch resp.StatusCode() {
	case http.StatusOK:
		apikeyProvider, err := securityprovider.NewSecurityProviderApiKey("header", "token", resp.JSON200.Token)
		if err != nil {
			panic(err)
		}
		clntOpts = append(clntOpts, WithRequestEditorFn(apikeyProvider.Intercept))

		api, err := NewClientWithResponses(server, clntOpts...)
		if err != nil {
			panic(err)
		}

		return api
	default:
		panic(fmt.Errorf("auth error: %s [%d]", resp.JSONDefault.Response, resp.JSONDefault.Code))
	}
}
