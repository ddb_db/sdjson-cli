package commands

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

type CUPrograms struct {
	Limit int `default:"500" help:"Max number of programs to delete in single run. Set to <1 for no limit, which can be slow."`
}

func (cmd CUPrograms) Run(db *sqlx.DB, log wlog.Logger) error {
	qry := cmd.bldQry()
	r, err := db.Exec(qry)
	if err != nil {
		return fmt.Errorf("cleanup(programs): %w", err)
	}
	log.Infof("%d program(s) removed", lo.Must(r.RowsAffected()))
	return nil
}

func (cmd CUPrograms) bldQry() string {
	if cmd.Limit < 1 {
		return "DELETE FROM programs WHERE program_id NOT IN (SELECT DISTINCT program_id FROM schedule)"
	}
	return fmt.Sprintf("DELETE FROM programs WHERE program_id IN (SELECT program_id FROM programs WHERE program_id NOT IN (SELECT DISTINCT program_id FROM schedule) LIMIT %d)", cmd.Limit)
}
