package commands

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/rossmerr/bitvector"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

type VASchedules struct{}

func (cmd VASchedules) Run(db *sqlx.DB, log wlog.Logger) error {
	defer fntimer.New().Start()("cmd:validate:schedules")
	log.Info("validate: checking for schedule overlaps")
	type schedRow struct {
		StationID string
		ProgramID string
		Start     int
		End       int
	}
	rows := make([]schedRow, 0)

	type sched struct {
		base     int
		timeline bitvector.BitVector
	}
	scheds := map[string]sched{}
	rs, err := db.Query("SELECT station_id AS s, program_id AS p, unixepoch(start_time) AS start, unixepoch(end_time) AS end FROM schedule ORDER BY start ASC")
	if err != nil {
		return fmt.Errorf("validate: sched overlap: %w", err)
	}
	for rs.Next() {
		row := schedRow{}
		rs.Scan(&row.StationID, &row.ProgramID, &row.Start, &row.End)
		rows = append(rows, row)
	}
	rs.Close()

	const maxTimeline = 24 * 60 * 30 // support upto 30 days of data per channel
	for _, r := range rows {
		s, ok := scheds[r.StationID]
		if !ok {
			s = sched{
				base:     r.Start,
				timeline: *bitvector.NewBitVector(maxTimeline),
			}
			scheds[r.StationID] = s
		}
		start := (r.Start - s.base) / 60
		end := (r.End - s.base) / 60
		end = lo.Ternary(end > maxTimeline, maxTimeline, end)
		itr := s.timeline.EnumerateFromOffset(start, end-1)
		for itr.HasNext() {
			if b, _ := itr.Next(); b {
				log.Warningf("validate: overlap detected: %s:%s %s => %s", r.StationID, r.ProgramID, time.Unix(int64(r.Start), 0), time.Unix(int64(r.End), 0))
				break
			}
		}
		for i := start; i < end; i++ {
			s.timeline.Set(i, true)
		}
	}
	return nil
}
