package commands

import "net/url"

type CLI struct {
	Server                 *url.URL `default:"https://json.schedulesdirect.org/20141201" help:"Schedules Direct API server URL."`
	Login                  string   `help:"Schedules Direct login id; saved in db file when specified, pulled from db file when not specified."`
	Password               string   `help:"Schedules Direct password; saved in db file when specified, pulled from db file when not specified."`
	DBFile                 string   `default:"sdjson.db" type:"path" help:"Path to local cache file where EPG data is stored."`
	NoCreateDB             bool     `default:"false" help:"Refuse to start if specified db file does not already exist (i.e. don't create a new db)."`
	Verbose                int      `type:"counter" short:"v" xor:"loglvl" help:"Increase logging level; multiple -v accepted."`
	Quiet                  int      `type:"counter" short:"q" xor:"loglvl" help:"Decrease logging level; multiple -q accepted."`
	DebugDbCalls           bool     `default:"false" help:"Log all database calls to a log file."`
	DebugRestCalls         bool     `default:"false" help:"Log all REST requests and responses to a temp directory for inspection."`
	MeasureFunctionRuntime []string `default:"_" help:"Log results of defined function timers throughout the code (debug option)."`
	Cleanup                Cleanup  `cmd:"" group:"Maintenance"`
	Fetch                  Fetch    `cmd:"" group:"Data Management" help:"Pull down latest EPG data into the local cache file."`
	Lineup                 Lineup   `cmd:"" group:"Account Management"`
	Search                 Search   `cmd:"" group:"Search"`
	Status                 Status   `cmd:"" group:"Account Management" help:"Dump Schedules Direct account status info to terminal."`
	Validate               Validate `cmd:"" group:"Maintenance"`
	Version                Version  `cmd:"" help:"Show version info and exit."`
}
