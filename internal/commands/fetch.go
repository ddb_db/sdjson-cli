package commands

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/cache"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/datastore"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

type Fetch struct {
	DBBatchSize          int `name:"db-batch" default:"750"`
	ScheduleMD5PoolSize  int `name:"sched-md5-pool" default:"16"`
	ScheduleMD5BatchSize int `name:"sched-md5-batch" default:"200"`
	SchedulePoolSize     int `name:"sched-pool" default:"16"`
	ScheduleBatchSize    int `name:"sched-batch" default:"500"` // 500 ~= 57s
	ProgPoolSize         int `name:"prog-pool-size" default:"16"`
	ProgBatchSize        int `name:"prog-batch-size" default:"5000"`
	ScheduleCleanupLimit int `name:"sched-cleanup-limit" default:"2500"`
	ScheduleCleanupDays  int `name:"sched-cleanup-days" default:"2"`

	api sdjson.ClientWithResponsesInterface
	db  *sqlx.DB
	cac cache.Cache
	log wlog.Logger
}

func (cmd Fetch) Run(api sdjson.ClientWithResponsesInterface, db *sqlx.DB, cac cache.Cache, log wlog.Logger) error {
	defer fntimer.New().Start()("cmd:fetch")
	cmd.api = sdjson.NewBatcherClient(api,
		cmd.ScheduleMD5PoolSize,
		cmd.ScheduleMD5BatchSize,
		cmd.SchedulePoolSize,
		cmd.ScheduleBatchSize,
		cmd.ProgPoolSize,
		cmd.ProgBatchSize)
	cmd.db = db
	cmd.cac = cac
	cmd.log = log

	if err := cmd.processLineups(); err != nil {
		return err
	}

	if err := cmd.fetchSchedules(); err != nil {
		return err
	}

	schedCleanup := CUSchedule{
		Limit: cmd.ScheduleCleanupLimit,
		Days:  cmd.ScheduleCleanupDays,
	}
	if err := schedCleanup.Run(db, log); err != nil {
		return err
	}

	return nil
}

func (cmd Fetch) fetchPrograms(ids []string) error {
	cmd.log.Infof("fetch: fetching %d programs", len(ids))
	progs, err := cmd.api.ProgramsWithResponse(context.TODO(), ids)
	if err != nil {
		return fmt.Errorf("fetch: program fetch: %w", err)
	}
	if progs.JSON200 == nil {
		return fmt.Errorf("fetch: program fetch: %s [%d]", progs.JSONDefault.Response, progs.StatusCode())
	}

	type progRow struct {
		ProgramID string `db:"program_id"`
		MD5       string `db:"md5"`
		Payload   []byte `db:"payload"`
	}
	rows := lo.Map(*progs.JSON200, func(p sdjson.Program, idx int) progRow {
		enc, err := json.Marshal(p)
		if err != nil {
			panic(err)
		}
		return progRow{p.ProgramID, p.Md5, enc}
	})

	tx, _ := cmd.db.Beginx()
	defer tx.Rollback()
	if err := datastore.Batch(tx.NamedExec,
		`INSERT INTO programs (program_id, md5, payload) VALUES (:program_id, :md5, :payload)
			ON CONFLICT (program_id) DO UPDATE SET md5=excluded.md5, payload=excluded.payload`,
		rows,
		cmd.DBBatchSize,
		"prog_insert"); err != nil {
		return fmt.Errorf("fetch: prog insert: %w", err)
	}
	return tx.Commit()
}

func (cmd Fetch) fetchSchedules() error {
	lineupStations := cmd.cac.LineupStations()
	ids := make(sdjson.ScheduleMD5JSONRequestBody, 0, lineupStations.Len())
	itr := lineupStations.Iterator()
	for !itr.Done() {
		id, _ := itr.Next()
		ids = append(ids, sdjson.StationRequest{StationID: id})
	}

	cmd.log.Infof("fetch: fetching %d schedules", len(ids))
	md5Resp, err := cmd.api.ScheduleMD5WithResponse(context.TODO(), ids)
	if err != nil {
		return fmt.Errorf("fetch: md5 check: %w", err)
	}
	if md5Resp.JSON200 == nil {
		return fmt.Errorf("fetch: md5 check: %s [%d]", md5Resp.JSONDefault.Response, md5Resp.StatusCode())
	}

	type schedStatusRow struct {
		StationID string    `db:"station_id"`
		SchedDate string    `db:"sched_date"`
		Md5       string    `db:"md5"`
		Modified  time.Time `db:"modified"`
	}

	schedules := make(map[string]sdjson.StationRequest)
	// prealloc 18 days per station id; SDJSON returns upto 18 days per station; preallocating 33% of the days and 25% of the station ids as an "average" case (pure guess)
	statuses := make([]schedStatusRow, 0, (18/3)*len(*md5Resp.JSON200)/4)
	timer := fntimer.New()
	timer.Start()
	for id, days := range *md5Resp.JSON200 {
		for d, s := range days {
			if cmd.cac.ScheduleMD5(id, d) != s.Md5 {
				req, ok := schedules[id]
				if !ok {
					dates := make([]string, 0, 18/3) // SDJSON returns upto 18 days per station; prealloc 33% of the days as avg case (pure guess)
					req = sdjson.StationRequest{
						StationID: id,
						Date:      &dates,
					}
					schedules[id] = req
				}
				*req.Date = append(*req.Date, d)
				statuses = append(statuses, schedStatusRow{id, d, s.Md5, s.LastModified})
			}
			timer.Split()
		}
	}
	timer.Stop("fetch:build_structs", true)
	if len(schedules) == 0 {
		cmd.log.Info("fetch: no schedule changes detected")
		return nil
	}

	schedResp, err := cmd.api.SchedulesWithResponse(context.TODO(), lo.Values(schedules))
	if err != nil {
		return fmt.Errorf("fetch: schedule get: %w", err)
	}
	if schedResp.JSONDefault != nil {
		return fmt.Errorf("fetch: schedule get: %s", schedResp.JSONDefault.Response)
	}
	sched := *schedResp.JSON200

	type airingRow struct {
		StationID string `db:"station_id"`
		Payload   []byte `db:"payload"`
	}

	neededProgramIDs := make(map[string]struct{}, 5000)
	airings := make([]airingRow, 0, len(sched)*30) // guessing there is ~30 programs scheduled on a channel per day
	for _, s := range sched {
		for _, p := range *s.Programs {
			if p.Md5 != cmd.cac.ProgramMD5(p.ProgramID) { // TODO how much gain from pulling the map once from cache then accessing it vs. this fn call
				neededProgramIDs[p.ProgramID] = struct{}{}
			}
			enc, err := json.Marshal(p)
			if err != nil {
				panic(err)
			}
			airings = append(airings, airingRow{s.StationID, enc})
		}
	}

	progIDs := lo.Keys(neededProgramIDs)
	if err := cmd.fetchPrograms(progIDs); err != nil {
		return err
	}

	cmd.log.Infof("fetch: saving %d airings to the schedule", len(airings))

	tx, _ := cmd.db.Beginx()
	defer tx.Rollback()
	if err := datastore.Batch(tx.NamedExec,
		`INSERT INTO schedule_status (station_id, sched_date, md5, modified) VALUES (:station_id, :sched_date, :md5, :modified)
			ON CONFLICT (station_id, sched_date) DO UPDATE SET md5=excluded.md5, modified=excluded.modified`,
		statuses,
		cmd.DBBatchSize,
		"sched_status_insert"); err != nil {
		return fmt.Errorf("fetch: schedule_status insert: %w", err)
	}

	type schedContraint struct {
		StationID string    `db:"id"`
		Start     time.Time `db:"s"`
		End       time.Time `db:"e"`
	}
	schedsToDelete := lo.Map(lo.Keys(lo.GroupBy(statuses, func(item schedStatusRow) string {
		return fmt.Sprintf("%s:%s", item.StationID, item.SchedDate)
	})), func(item string, idx int) schedContraint {
		vals := strings.SplitN(item, ":", 2)
		start, end := computeStartAndEnd(vals[1])
		return schedContraint{
			StationID: vals[0],
			Start:     start,
			End:       end,
		}
	})
	for _, s := range schedsToDelete {
		_, err := tx.NamedExec("DELETE FROM schedule WHERE station_id = :id AND julianday(start_time) >= julianday(:s) AND julianday(start_time) < julianday(:e)", s)
		if err != nil {
			return fmt.Errorf("fetch: schedule delete: %w", err)
		}
	}
	if err := datastore.Batch(tx.NamedExec, `INSERT INTO schedule (station_id, payload) VALUES (:station_id, :payload)`, airings, cmd.DBBatchSize, "sched_insert"); err != nil {
		return fmt.Errorf("fetch: schedule insert: %w", err)
	}
	return tx.Commit()
}

func computeStartAndEnd(d string) (time.Time, time.Time) {
	start, err := time.Parse("2006-01-02", d)
	if err != nil {
		panic(err)
	}
	return start, start.Add(time.Hour * 24)
}

func (cmd Fetch) processLineups() error {
	type lineupStationsRow struct {
		L string
		S string
		P []byte
	}

	type stationRow struct {
		S string
		P []byte
	}

	resp, err := cmd.api.LineupsWithResponse(context.TODO())
	if err != nil {
		return fmt.Errorf("fetch: %w", err)
	}
	if resp.JSONDefault != nil {
		return fmt.Errorf("fetch: lineups api: %s", resp.JSONDefault.Response)
	}

	for _, lineup := range resp.JSON200.Lineups {
		if err := func() error {
			id := lineup.Lineup
			detailsResp, err := cmd.api.LineupWithResponse(context.TODO(), id)
			if err != nil {
				return fmt.Errorf("fetch: lineup api error: %w", err)
			}
			if detailsResp.JSONDefault != nil {
				return fmt.Errorf("fetch: lineup api failed: %s", detailsResp.JSONDefault.Response)
			}
			details := detailsResp.JSON200

			stations := make([]stationRow, 0, len(details.Stations))
			for _, s := range details.Stations {
				enc, err := json.Marshal(s)
				if err != nil {
					panic(err)
				}
				stations = append(stations, stationRow{s.StationID, enc})
			}

			lineupStations := make([]lineupStationsRow, 0, len(details.Map))
			for _, m := range details.Map {
				enc, err := json.Marshal(m)
				if err != nil {
					panic(err)
				}
				lineupStations = append(lineupStations, lineupStationsRow{id, m.StationID, enc})
			}

			tx, err := cmd.db.Beginx()
			if err != nil {
				return fmt.Errorf("fetch: tx begin: %w", err)
			}
			defer tx.Rollback()

			if err := datastore.Batch(
				tx.NamedExec,
				"INSERT INTO stations (station_id, payload) VALUES (:s, :p) ON CONFLICT (station_id) DO UPDATE SET payload=excluded.payload",
				stations,
				cmd.DBBatchSize,
				"stations_insert"); err != nil {
				return fmt.Errorf("fetch: stations insert: %w", err)
			}

			res, err := tx.Exec(`INSERT INTO lineups (lineup_id, modified) VALUES (?, ?)
					ON CONFLICT (lineup_id) DO UPDATE SET modified=excluded.modified
					WHERE julianday(modified) < julianday(excluded.modified)`, lineup.Lineup, details.Metadata.Modified)
			if err != nil {
				return fmt.Errorf("fetch: lineups insert: %w", err)
			}

			c, err := res.RowsAffected()
			if err != nil {
				return fmt.Errorf("fetch: rows affected: %w", err)
			}

			if c == 1 {
				if _, err := tx.Exec(`DELETE FROM lineup_stations WHERE lineup_id = ?`, id); err != nil {
					return fmt.Errorf("fetch: lineup_stations delete: %w", err)
				}
				if err := datastore.Batch(
					tx.NamedExec,
					"INSERT INTO lineup_stations (lineup_id, station_id, payload) VALUES (:l, :s, :p)",
					lineupStations,
					cmd.DBBatchSize,
					"lineup_stations_insert"); err != nil {
					return fmt.Errorf("fetch: lineup_stations insert: %w", err)
				}
			}
			return tx.Commit()
		}(); err != nil {
			return err
		}
	}
	return nil
}
