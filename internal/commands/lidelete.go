package commands

import (
	"context"
	"fmt"

	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
)

type LIDelete struct {
	Id string `required:"true" help:"ID to remove from account; see status for currently registered ids."`
}

func (cmd LIDelete) Run(api sdjson.ClientWithResponsesInterface) error {
	resp, err := api.DeleteLineupWithResponse(context.TODO(), cmd.Id)
	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		return nil
	}
	if resp.JSONDefault != nil {
		fmt.Printf("ERROR: %s\n", resp.JSONDefault.Response)
		return nil
	}
	if resp.JSON200.Code != 0 {
		fmt.Printf("ERROR: %s\n", resp.JSON200.Response)
		return nil
	}
	fmt.Println(resp.JSON200.Response)
	return nil
}
