package commands

type Lineup struct {
	Add    LIAdd    `cmd:"" help:"Add a new lineup to your account. See status output for limits."`
	Delete LIDelete `cmd:"" help:"Remove lineup from your account. Does not remove previously downloaded data."`
}
