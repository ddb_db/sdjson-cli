package commands

type Search struct {
	Countries SECountries `cmd:"" help:"List supported countries for EPG data."`
	Lineups   SELineups   `cmd:"" help:"Search for lineups based on country and postal code."`
}
