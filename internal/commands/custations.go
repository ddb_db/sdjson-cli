package commands

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

type CUStations struct {
	Limit int `default:"100" help:"Max number of stations to delete in single run. Set to <1 for no limit, which can be slow."`
}

func (cmd CUStations) Run(db *sqlx.DB, log wlog.Logger) error {
	qry := cmd.bldQry()
	r, err := db.Exec(qry)
	if err != nil {
		return fmt.Errorf("cleanup(stations): %w", err)
	}
	log.Infof("%d station(s) removed", lo.Must(r.RowsAffected()))
	return nil
}

func (cmd CUStations) bldQry() string {
	if cmd.Limit < 1 {
		return "DELETE FROM stations WHERE station_id NOT IN (SELECT DISTINCT station_id FROM lineup_stations)"
	}
	return fmt.Sprintf("DELETE FROM stations WHERE station_id IN (SELECT station_id FROM stations WHERE station_id NOT IN (SELECT DISTINCT station_id FROM lineup_stations) LIMIT %d)", cmd.Limit)
}
