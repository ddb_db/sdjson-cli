package commands

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/vargspjut/wlog"
)

type CUDB struct{}

func (cmd CUDB) Run(db *sqlx.DB, log wlog.Logger) error {
	log.Warning("This command is usually unnecessary. You should only run this command if you have a specific reason to do so.")
	_, err := db.Exec("VACUUM")
	if err != nil {
		return fmt.Errorf("cleanup(db): %w", err)
	}
	log.Info("db cleanup completed")
	return nil
}
