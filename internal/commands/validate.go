package commands

type Validate struct {
	Schedules VASchedules `cmd:"" default:"1" help:"Check all schedules for airings that overlap. See https://is.gd/nkLy3s for more details."`
}
