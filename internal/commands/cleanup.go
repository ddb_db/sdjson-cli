package commands

type Cleanup struct {
	DB       CUDB       `cmd:"" help:"Reclaim unused space in db file. See https://is.gd/3tIzY3 for more details."`
	Lineups  CULineups  `cmd:"" help:"Remove lineups from db file no longer registered in your SD account."`
	Programs CUPrograms `cmd:"" help:"Remove programs from db file no longer referenced in any schedules. See https://is.gd/N7sKva for more details."`
	Stations CUStations `cmd:"" help:"Remove stations from db file no longer referenced in any lineups. See https://is.gd/m5UbOi for more details."`
	Schedule CUSchedule `cmd:"" help:"Remove entries from the schedule that have expired (are in the past)."`
}
