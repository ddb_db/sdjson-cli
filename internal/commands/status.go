package commands

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
)

type Status struct{}

func (cmd Status) Run(api sdjson.ClientWithResponsesInterface) error {
	s, err := api.StatusWithResponse(context.TODO())
	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		return nil
	}
	enc := bytes.Buffer{}
	if err := json.Indent(&enc, s.Body, "", "  "); err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		return nil
	}
	fmt.Println(enc.String())
	return nil
}
