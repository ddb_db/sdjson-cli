package commands

import (
	"context"
	"fmt"
	"slices"
	"strings"

	"github.com/samber/lo"
	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
)

type SECountries struct {
	Filter string `help:"Filters results by country name containing given value."`
}

func (cmd SECountries) Run(api sdjson.ClientWithResponsesInterface) error {
	const cliLineFmt = "%-8s %-32s %s\n"

	resp, err := api.AvailableCountriesWithResponse(context.TODO())
	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		return nil
	}
	if resp.JSONDefault != nil {
		fmt.Printf("ERROR: %s\n", resp.JSONDefault.Response)
		return nil
	}

	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf(cliLineFmt, "ISO", "Country", "Single Postal Code"))
	sb.WriteString(strings.Repeat("=", 79) + "\n")
	matches := lo.Filter(lo.Flatten(lo.Values(*resp.JSON200)), func(c sdjson.Country, idx int) bool {
		return cmd.Filter == "" || strings.Contains(strings.ToLower(c.FullName), strings.ToLower(cmd.Filter))
	})
	slices.SortFunc(matches, func(a, b sdjson.Country) int {
		return strings.Compare(a.FullName, b.FullName)
	})
	lo.ForEach(matches, func(c sdjson.Country, idx int) {
		sb.WriteString(fmt.Sprintf(cliLineFmt, c.ShortName, c.FullName, lo.Ternary(!lo.IsNil(c.OnePostalCode) && *c.OnePostalCode, c.PostalCodeExample, "")))
	})
	fmt.Print(sb.String())
	return nil
}
