package commands

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
)

type CUSchedule struct {
	Limit int `default:"2500" help:"Max number of schedule entries to delete in single run. Set to <1 for no limit, which can be slow."`
	Days  int `default:"2" help:"Number of days to keep expired schedule entries for."`
}

func (cmd CUSchedule) Run(db *sqlx.DB, log wlog.Logger) error {
	maxEnd := time.Now().Add(time.Hour * time.Duration(-1*cmd.Days*24))
	qry := cmd.bldQry()
	r, err := db.Exec(qry, maxEnd)
	if err != nil {
		return fmt.Errorf("cleanup(schedule): %w", err)
	}
	log.Infof("%d schedule entries removed", lo.Must(r.RowsAffected()))
	return nil
}

func (cmd CUSchedule) bldQry() string {
	if cmd.Limit < 1 {
		return "DELETE FROM schedule WHERE julianday(end_time) < julianday(?)"
	}
	return fmt.Sprintf("DELETE FROM schedule WHERE id IN (SELECT id FROM schedule WHERE julianday(end_time) < julianday(?) LIMIT %d)", cmd.Limit)
}
