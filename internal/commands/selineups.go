package commands

import (
	"context"
	"fmt"
	"strings"

	"github.com/samber/lo"
	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
)

type SELineups struct {
	Country    string `required:"true" help:"ISO country code to search within; use search conuntries command for valid ISO codes."`
	PostalCode string `required:"true" help:"Postal code to search for; note that some countries only support a single postal code."`
}

func (cmd SELineups) Run(api sdjson.ClientWithResponsesInterface) error {
	const strfmt = "%-24s %s\n"

	resp, err := api.HeadendsWithResponse(context.TODO(), &sdjson.HeadendsParams{
		Country:    cmd.Country,
		Postalcode: cmd.PostalCode,
	})
	if err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		return nil
	}
	if resp.JSONDefault != nil {
		fmt.Printf("ERROR: %s\n", resp.JSONDefault.Response)
		return nil
	}

	sb := strings.Builder{}
	sb.WriteString(fmt.Sprintf(strfmt, "ID", "Name"))
	sb.WriteString(strings.Repeat("=", 79) + "\n")
	lo.ForEach(*resp.JSON200, func(h sdjson.Headend, idx int) {
		lo.ForEach(h.Lineups, func(item sdjson.HeadendLineup, index int) {
			sb.WriteString(fmt.Sprintf(strfmt, item.Lineup, fmt.Sprintf("%s: %s", h.Location, item.Name)))
		})
	})
	fmt.Print(sb.String())
	return nil
}
