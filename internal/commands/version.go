package commands

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	"github.com/samber/lo"
	"gitlab.com/ddb_db/sdjson-cli/internal/appruntime"
)

type Version struct{}

func (cmd Version) Run(info appruntime.AppInfo) error {
	fmt.Printf("%s %s %s %s\nhttps://gitlab.com/ddb_db/sdjson-cli\n",
		filepath.Base(filepath.Clean(os.Args[0])),
		info.Version,
		runtime.Version(),
		lo.Ternary(info.BuildDate != "", info.BuildDate, ""))
	return nil
}
