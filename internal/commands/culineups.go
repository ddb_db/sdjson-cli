package commands

import (
	"context"
	"fmt"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/samber/lo"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
)

type CULineups struct{}

func (cmd CULineups) Run(api sdjson.ClientWithResponsesInterface, db *sqlx.DB, log wlog.Logger) error {
	resp, err := api.LineupsWithResponse(context.TODO())
	if err != nil {
		return err
	}
	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("cleanup(lineups): %s", resp.Status())
	}

	if resp.JSON200 != nil {
		ids := lo.Map(resp.JSON200.Lineups, func(l sdjson.Lineup, idx int) string {
			return l.Lineup
		})
		qry, args, err := sqlx.In("DELETE FROM lineups WHERE lineup_id NOT IN (?)", ids)
		if err != nil {
			return err
		}
		r, err := db.Exec(qry, args...)
		if err == nil {
			log.Infof("%d lineup(s) removed", lo.Must(r.RowsAffected()))
		}
		return err
	}

	if resp.JSONDefault != nil {
		return fmt.Errorf("cleanup(lineups): %s", resp.JSONDefault.Response)
	}
	return nil
}
