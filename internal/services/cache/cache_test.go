package cache_test

import (
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/cache"
)

func TestLineupStationsHitsDBOnlyOnce(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock failed: %s", err)
	}
	defer db.Close()
	mock.ExpectQuery("").
		WillReturnRows(sqlmock.NewRows([]string{"station_id"}))
	dbx := sqlx.NewDb(db, "sqlmock")

	sut := cache.New(dbx)
	// first call pulls from db, second should not hit db
	sut.LineupStations()
	sut.LineupStations()
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestScheduleMD5HitsDBOnlyOnce(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatalf("sqlmock failed: %s", err)
	}
	defer db.Close()
	mock.ExpectQuery("").
		WillReturnRows(sqlmock.NewRows([]string{"station_id"}))
	dbx := sqlx.NewDb(db, "sqlmock")

	sut := cache.New(dbx)
	// first call pulls from db, second should not hit db
	sut.ScheduleMD5("foo", "bar")
	sut.ScheduleMD5("zoo", "baz")
	assert.NoError(t, mock.ExpectationsWereMet())
}
