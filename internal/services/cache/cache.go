package cache

import (
	imm "github.com/benbjohnson/immutable"
	"github.com/jmoiron/sqlx"
	"github.com/patrickmn/go-cache"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

type Cache interface {
	LineupStations() imm.Set[string]
	ScheduleMD5(schedID, date string) string
	ProgramMD5(progID string) string
}

func New(db *sqlx.DB) CacheImpl {
	c := cache.New(cache.NoExpiration, cache.NoExpiration)
	return CacheImpl{
		db:  db,
		cac: c,
	}
}

const (
	keyLineupStations = "lineupStations"
	keyScheduleMD5    = "scheduledMD5"
	keyProgramMD5s    = "programMD5s"
)

type CacheImpl struct {
	db  *sqlx.DB
	cac *cache.Cache
}

type programMD5s map[string]string

func (impl CacheImpl) ProgramMD5(progID string) string {
	v, ok := impl.cac.Get(keyProgramMD5s)
	if !ok {
		v = impl.fetchProgramMD5s()
		impl.cac.Set(keyProgramMD5s, v, cache.DefaultExpiration)
	}
	m := v.(programMD5s)
	return m[progID]
}

func (impl CacheImpl) LineupStations() imm.Set[string] {
	v, ok := impl.cac.Get(keyLineupStations)
	if !ok {
		v = impl.fetchLineupStations()
		impl.cac.Set(keyLineupStations, v, cache.DefaultExpiration)
	}
	return v.(imm.Set[string])
}

type schedMD5Map map[string]map[string]string

func (impl CacheImpl) ScheduleMD5(stationID, date string) string {
	v, ok := impl.cac.Get(keyScheduleMD5)
	if !ok {
		v = impl.fetchScheduleMD5s()
		impl.cac.Set(keyScheduleMD5, v, cache.DefaultExpiration)
	}
	m := v.(schedMD5Map)
	if s, ok := m[stationID]; ok {
		return s[date]
	}
	return ""
}

func (impl CacheImpl) fetchScheduleMD5s() schedMD5Map {
	defer fntimer.New().Start()("cache:fetchScheduleMD5s")
	rows, err := impl.db.Query("SELECT station_id, sched_date, md5 FROM schedule_status")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	vals := make(schedMD5Map)
	for rows.Next() {
		id := new(string)
		date := new(string)
		md5 := new(string)
		if err := rows.Scan(id, date, md5); err != nil {
			panic(err)
		}
		s, ok := vals[*id]
		if !ok {
			s = make(map[string]string)
			vals[*id] = s
		}
		s[*date] = *md5
	}
	return vals
}

func (impl CacheImpl) fetchProgramMD5s() programMD5s {
	defer fntimer.New().Start()("cache:fetchProgMD5s")
	rows, err := impl.db.Queryx("SELECT program_id, md5 FROM programs")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	// TODO how much of gain is there if we do a COUNT(*) to properly allocate
	vals := make(programMD5s)
	for rows.Next() {
		id := new(string)
		md5 := new(string)
		if err := rows.Scan(id, md5); err != nil {
			panic(err)
		}
		vals[*id] = *md5
	}
	return vals
}

func (impl CacheImpl) fetchLineupStations() imm.Set[string] {
	defer fntimer.New().Start()("cache:fetchLineupStations")
	rows, err := impl.db.Query("SELECT DISTINCT station_id FROM lineup_stations")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	vals := make([]string, 0, 500)
	for rows.Next() {
		val := new(string)
		if err := rows.Scan(val); err != nil {
			panic(err)
		}
		vals = append(vals, *val)
	}
	return imm.NewSet(nil, vals...)
}
