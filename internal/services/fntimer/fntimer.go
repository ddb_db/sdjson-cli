package fntimer

import (
	"errors"
	"fmt"
	"slices"
	"strings"
	"time"

	"github.com/vargspjut/wlog"
)

var EnabledCategories []string = []string{""}

var zeroDuration time.Duration = time.Duration(-1)

type Stopwatch interface {
	Start() func() RunDetails
	Split() SplitDetails
	Stop(cancelLastSplit ...bool) RunDetails
}

type SplitDetails struct {
	Duration time.Duration
	Lap      int
}

type RunDetails struct {
	Duration       time.Duration
	Laps           int
	MaxLapDuration time.Duration
	MinLapDuration time.Duration
	AvgLapDuration time.Duration
	MinLap         int
	MaxLap         int
}

func (rd RunDetails) String() string {
	sb := make([]string, 0, 5)
	sb = append(sb, fmt.Sprintf("d=%s", rd.Duration))
	if rd.Laps > 1 {
		sb = append(sb, fmt.Sprintf("min=%v [%d]", rd.MinLapDuration, rd.MinLap))
		sb = append(sb, fmt.Sprintf("max=%v [%d]", rd.MaxLapDuration, rd.MaxLap))
		sb = append(sb, fmt.Sprintf("avg=%s", rd.AvgLapDuration))
		sb = append(sb, fmt.Sprintf("itr=%d", rd.Laps))
	}
	return fmt.Sprintf("{%s}", strings.Join(sb, ","))
}

func New() *StopwatchImpl {
	return &StopwatchImpl{
		maxLapDur: zeroDuration,
		minLapDur: zeroDuration,
	}
}

type StopwatchImpl struct {
	isRunning  bool
	laps       int
	runStart   time.Time
	splitStart time.Time
	minLapDur  time.Duration
	maxLapDur  time.Duration
	minLap     int
	maxLap     int
	runEnd     time.Duration
}

func (impl *StopwatchImpl) Start() func(string, ...bool) RunDetails {
	if impl.isRunning {
		panic(errors.New("fntimer: can't start a running watch"))
	}
	impl.isRunning = true
	impl.runStart = time.Now()
	impl.splitStart = impl.runStart
	return impl.Stop
}

func (impl *StopwatchImpl) Split() SplitDetails {
	now := time.Now()
	if !impl.isRunning {
		panic(errors.New("fntimer: can't split a stopped watch"))
	}
	impl.laps++
	dur := time.Since(impl.splitStart)
	impl.splitStart = now
	if impl.minLapDur == zeroDuration || impl.minLapDur > dur {
		impl.minLapDur = dur
		impl.minLap = impl.laps
	}
	if impl.maxLapDur == zeroDuration || impl.maxLapDur < dur {
		impl.maxLapDur = dur
		impl.maxLap = impl.laps
	}
	return SplitDetails{
		Duration: dur,
		Lap:      impl.laps,
	}
}

func (impl *StopwatchImpl) Stop(lbl string, cancelLastSplit ...bool) RunDetails {
	if !impl.isRunning {
		return RunDetails{
			Duration:       impl.runEnd,
			Laps:           impl.laps,
			MaxLapDuration: impl.maxLapDur,
			MinLapDuration: impl.minLapDur,
			AvgLapDuration: time.Duration(impl.runEnd / time.Duration(impl.laps)),
		}
	}

	if impl.isRunning {
		now := time.Now()
		impl.runEnd = now.Sub(impl.runStart)
		if impl.laps == 0 || len(cancelLastSplit) == 0 || !cancelLastSplit[0] {
			impl.laps++
			dur := now.Sub(impl.splitStart)
			if impl.minLapDur == zeroDuration || impl.minLapDur > dur {
				impl.minLapDur = dur
				impl.minLap = impl.laps
			}
			if impl.maxLapDur == zeroDuration || impl.maxLapDur < dur {
				impl.maxLapDur = dur
				impl.maxLap = impl.laps
			}
		}
	}
	impl.isRunning = false
	info := RunDetails{
		Duration:       impl.runEnd,
		Laps:           impl.laps,
		MaxLapDuration: impl.maxLapDur,
		MinLapDuration: impl.minLapDur,
		AvgLapDuration: time.Duration(impl.runEnd / time.Duration(impl.laps)),
		MinLap:         impl.minLap,
		MaxLap:         impl.maxLap,
	}
	if lbl != "" {
		cat := strings.SplitN(lbl, ":", 2)[0]
		if EnabledCategories[0] == "*" || slices.Contains(EnabledCategories, cat) {
			wlog.WithScope(wlog.Fields{"stats": info.String()}).Debugf("fntimer: %s", lbl)
		}
	}
	return info
}
