package datastore_test

import (
	"database/sql"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/datastore"
)

const tmpDBFile = "foo.db"

func TestPanicIfAllowCreateIsFalse(t *testing.T) {
	f := tmpDBFile
	assert.NoFileExists(t, f)
	assert.Panics(t, func() { datastore.New(f, false, false) })
}

func TestNoPanicIfAllowCreateIsTrue(t *testing.T) {
	f := tmpDBFile
	assert.NoFileExists(t, f)
	defer os.Remove(f)

	datastore.New(f, true, false)
	defer datastore.Get().Close()
	assert.FileExists(t, tmpDBFile)
}

func TestBatchSplitsInputProperly(t *testing.T) {
	fnCount := 0
	fn := func(qry string, args any) (sql.Result, error) {
		fnCount++
		return nil, nil
	}

	input := []int{0, 1, 2, 3}
	err := datastore.Batch(fn, "", input, 1, "foo")
	assert.NoError(t, err)
	assert.Equal(t, len(input), fnCount)
}
