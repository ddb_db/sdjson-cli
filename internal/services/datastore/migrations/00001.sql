-- +migrate Up
CREATE TABLE settings (
    name VARCHAR(128) PRIMARY KEY NOT NULL,
    value TEXT NOT NULL
);

CREATE TABLE stations (
    id INTEGER PRIMARY KEY NOT NULL,
    station_id VARCHAR(64) NOT NULL UNIQUE,
    payload BLOB NOT NULL
);

CREATE TABLE lineups (
    id INTEGER PRIMARY KEY NOT NULL,
    lineup_id VARCHAR(64) NOT NULL UNIQUE,
    modified DATETIME NOT NULL
);

CREATE TABLE lineup_stations (
    id INTEGER PRIMARY KEY NOT NULL,
    lineup_id VARCHAR(64) NOT NULL,
    station_id VARCHAR(64) NOT NULL,
    payload BLOB NOT NULL,
    FOREIGN KEY(lineup_id) REFERENCES lineups(lineup_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(station_id) REFERENCES stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE schedule_status (
    station_id VARCHAR(64) NOT NULL,
    sched_date CHAR(10) NOT NULL,
    md5 VARCHAR(64) NOT NULL,
    modified DATETIME NOT NULL,
    PRIMARY KEY (station_id, sched_date),
    FOREIGN KEY(station_id) REFERENCES stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE programs (
    id INTEGER PRIMARY KEY NOT NULL,
    program_id VARCHAR(64) NOT NULL UNIQUE,
    md5 VARCHAR(64) NOT NULL,
    payload BLOB NOT NULL
);

CREATE TABLE schedule (
    id INTEGER PRIMARY KEY NOT NULL,
    station_id VARCHAR(64) NOT NULL,
    program_id VARCHAR(64) GENERATED ALWAYS AS (json_extract(payload, '$.programID')) STORED NOT NULL,
    start_time DATETIME GENERATED ALWAYS AS (json_extract(payload, '$.airDateTime')) STORED NOT NULL,
    end_time DATETIME GENERATED ALWAYS AS (datetime(start_time, json_extract(payload, '$.duration') || ' seconds')) STORED NOT NULL,
    payload BLOB NOT NULL,
    FOREIGN KEY(station_id) REFERENCES stations(station_id) ON DELETE CASCADE ON UPDATE CASCADE
    FOREIGN KEY(program_id) REFERENCES programs(program_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE INDEX "idx_sched_start" ON "schedule" (
	"station_id",
	julianday(start_time)
);

CREATE INDEX "idx_lineup_sta_lineup_id" ON "lineup_stations" (
	"lineup_id"
);

CREATE INDEX "idx_lineup_sta_station_id" ON "lineup_stations" (
	"station_id"
);

CREATE INDEX "idx_sched_program_id" ON "schedule" (
	"program_id"
);
