-- +migrate Up
CREATE INDEX "idx_sched_end_time" ON "schedule" (
	julianday(end_time)
);
