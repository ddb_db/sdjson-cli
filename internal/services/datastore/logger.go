package datastore

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	sqldblogger "github.com/simukti/sqldb-logger"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/appruntime"
)

var logFile *os.File

func InitLogger(create bool) {
	if !create || appruntime.LogDir == "" {
		return
	}

	f, err := os.Create(filepath.Clean(fmt.Sprintf("%s/sdjson-cli.db.log", appruntime.LogDir)))
	if err != nil {
		panic(err)
	}
	logFile = f
	wlog.Debugf("DB LOG: %s", logFile.Name())
}

func NewLogger() sqldblogger.Logger {
	return &dbLogger{
		mu: sync.Mutex{},
	}
}

type dbLogger struct {
	mu sync.Mutex
}

func (log *dbLogger) Log(ctx context.Context, lvl sqldblogger.Level, msg string, data map[string]any) {
	// TODO signal a shutdown and close the file properly; maybe not needed with Sync()?
	log.mu.Lock()
	defer log.mu.Unlock()
	enc, _ := json.Marshal(data)
	logFile.WriteString(fmt.Sprintf("==\n%s\n--\n%s\n", msg, string(enc)))
	logFile.Sync()
}
