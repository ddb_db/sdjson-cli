package datastore

import (
	"database/sql"
	"embed"
	"errors"
	"fmt"
	"os"

	_ "github.com/glebarez/go-sqlite"
	"github.com/jmoiron/sqlx"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/samber/lo"
	sqldblogger "github.com/simukti/sqldb-logger"
	"github.com/vargspjut/wlog"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
)

//go:embed migrations/*
var migrations embed.FS

var instance *sqlx.DB

func New(dsn string, allowCreate bool, logEnabled bool) *sqlx.DB {
	if instance != nil && dsn != ":memory:" {
		return instance
	}

	if dsn != ":memory:" {
		checkFile(dsn, allowCreate)
	}
	wlog.Debugf("Working with: %s", dsn)

	fulldsn := fmt.Sprintf("%s?_pragma=foreign_keys(1)&_pragma=busy_timeout(2000)", dsn)
	db, err := sql.Open("sqlite", fulldsn)
	if err != nil {
		panic(err)
	}
	if logEnabled && wlog.DefaultLogger().GetLogLevel() == wlog.Dbg {
		db = sqldblogger.OpenDriver(fulldsn, db.Driver(), NewLogger())
	}
	dbx := sqlx.NewDb(db, "sqlite")

	src := migrate.EmbedFileSystemMigrationSource{
		FileSystem: migrations,
		Root:       "migrations",
	}

	if _, err := migrate.Exec(dbx.DB, "sqlite3", src, migrate.Up); err != nil {
		panic(err)
	}
	instance = dbx
	return instance
}

func Get() *sqlx.DB {
	if instance == nil {
		panic(fmt.Errorf("datastore: must call New() before Get()"))
	}
	return instance
}

func Batch[T any](fn func(qry string, args any) (sql.Result, error), qry string, args []T, size int, name string) error {
	defer fntimer.New().Start()("datastore:batch:"+name, true)
	batches := lo.Chunk(args, size)
	for i, b := range batches {
		if _, err := fn(qry, b); err != nil {
			return fmt.Errorf("datastore: batch[%s] [idx=%d]: %w", name, i, err)
		}
	}
	return nil
}

func BatchIn[T comparable](tx *sqlx.Tx, qry string, args []T, size int, name string) error {
	defer fntimer.New().Start()("datastore:batch_in:"+name, true)
	batches := lo.Chunk(args, size)
	for i, b := range batches {
		batchQry, batchArgs, err := sqlx.In(qry, b)
		if err != nil {
			return fmt.Errorf("datastore: batch_in[%s] [idx=%d]: %w", name, i, err)
		}
		batchQry = tx.Rebind(batchQry)
		if _, err := tx.Exec(batchQry, batchArgs...); err != nil {
			return fmt.Errorf("datastore: batch_in[%s] [idx=%d]: %w", name, i, err)
		}
	}
	return nil
}

func checkFile(fileName string, allowCreate bool) {
	info, err := os.Stat(fileName)
	if err != nil {
		if !errors.Is(err, os.ErrNotExist) {
			panic(err)
		}
		if !allowCreate {
			panic(fmt.Errorf("io error: specified db file does not exist and --no-create specified"))
		}
	} else if !info.Mode().IsRegular() {
		panic(fmt.Errorf("io error: specified db file already exists and is not a regular file"))
	}
}
