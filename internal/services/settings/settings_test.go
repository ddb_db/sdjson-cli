package settings_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/datastore"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/settings"
)

func TestSetAndGetActAsExpected(t *testing.T) {
	db := datastore.New(":memory:", true, false)
	defer db.Close()
	sut := settings.New(db)
	sut.Set("foo", "bar")
	assert.Equal(t, "bar", sut.Get("foo"))
}

func TestGetWithDefaultReturnsDefaultAsExpected(t *testing.T) {
	db := datastore.New(":memory:", true, false)
	defer db.Close()
	sut := settings.New(db)
	assert.Nil(t, sut.Get("foo"))
	assert.Equal(t, "bar", sut.GetWithDefault("foo", "bar"))
}
