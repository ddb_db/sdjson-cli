package settings

import (
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"
)

type SettingsService interface {
	Get(name string) any
	GetWithDefault(name string, defaultVal any) any
	GetString(name string) string
	GetStringWithDefault(name, defaultVal string) string
	Set(name string, value any)
}

func New(db *sqlx.DB) SettingsImpl {
	return SettingsImpl{
		db: db,
	}
}

type SettingsImpl struct {
	db *sqlx.DB
}

func (svc SettingsImpl) Set(name string, value any) {
	if _, err := svc.db.Exec("INSERT INTO settings (name, value) VALUES (?, ?) ON CONFLICT (name) DO UPDATE SET value = excluded.value", name, value); err != nil {
		panic(err)
	}
}

func (svc SettingsImpl) GetWithDefault(name string, defaultVal any) any {
	var result any
	if err := svc.getIntoWithDefault(&result, name); err != nil {
		return defaultVal
	}
	return result
}

func (svc SettingsImpl) Get(name string) any {
	return svc.GetWithDefault(name, nil)
}

func (svc SettingsImpl) GetStringWithDefault(name, defaultVal string) string {
	result := new(string)
	if err := svc.getIntoWithDefault(result, name); err != nil {
		return defaultVal
	}
	return *result
}

func (svc SettingsImpl) GetString(name string) string {
	return svc.GetStringWithDefault(name, "")
}

func (svc SettingsImpl) getIntoWithDefault(dst any, name string) error {
	if err := svc.db.Get(dst, "SELECT value FROM settings WHERE name = ?", name); err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			panic(err)
		}
		return err
	}
	return nil
}
