package security_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/ddb_db/sdjson-cli/internal/security"
	"gitlab.com/ddb_db/sdjson-cli/mocks"
)

func TestCLIValuesTakesPriority(t *testing.T) {
	cliLogin := "foo"
	cliPwd := "bar"

	svc := mocks.NewSettingsService(t)
	svc.EXPECT().Set(mock.Anything, cliLogin)
	svc.EXPECT().Set(mock.Anything, mock.Anything)

	sut := security.NewSDJSONCreds(cliLogin, cliPwd, svc)
	assert.Equal(t, cliLogin, sut.Login())
	finalPwd := sut.Password()
	assert.NotEqual(t, "", finalPwd)
	assert.NotEqual(t, cliPwd, finalPwd) // because it's been hashed
}

func TestDBSettingsAreUsedWhenCLIIsNotSet(t *testing.T) {
	storedLogin := "foo"
	storedPwd := "bar"

	svc := mocks.NewSettingsService(t)
	svc.EXPECT().GetString(mock.Anything).Return(storedLogin).Once()
	svc.EXPECT().GetString(mock.Anything).Return(storedPwd).Once()
	svc.EXPECT().Set(mock.Anything, storedLogin)
	svc.EXPECT().Set(mock.Anything, mock.Anything)

	sut := security.NewSDJSONCreds("", "", svc)
	assert.Equal(t, storedLogin, sut.Login())
	assert.Equal(t, storedPwd, sut.Password())
}
