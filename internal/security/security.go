package security

import (
	"crypto/sha1"
	"encoding/hex"

	"gitlab.com/ddb_db/sdjson-cli/internal/services/settings"
)

const (
	sdjsonLoginKey = "sdjson.login"
	sdjsonPwdKey   = "sdjson.password"
)

type SDJSONCredsProvider interface {
	Login() string
	Password() string
}

type SDJSONCredsProviderImpl struct {
	login    string
	password string
}

func (creds SDJSONCredsProviderImpl) Login() string {
	return creds.login
}

func (creds SDJSONCredsProviderImpl) Password() string {
	return creds.password
}

func NewSDJSONCreds(cliLogin, cliPassword string, settingsSvc settings.SettingsService) SDJSONCredsProviderImpl {
	id := cliLogin
	pwd := cliPassword
	if cliLogin == "" {
		id = settingsSvc.GetString(sdjsonLoginKey)
	}
	if cliPassword == "" {
		pwd = settingsSvc.GetString(sdjsonPwdKey)
	} else {
		pwd = hashPassword(cliPassword)
	}
	settingsSvc.Set(sdjsonLoginKey, id)
	settingsSvc.Set(sdjsonPwdKey, pwd)
	return SDJSONCredsProviderImpl{
		login:    id,
		password: pwd,
	}
}

func hashPassword(in string) string {
	h := sha1.New()
	h.Write([]byte(in))
	return hex.EncodeToString(h.Sum(nil))
}
