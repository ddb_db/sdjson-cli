module gitlab.com/ddb_db/sdjson-cli

go 1.21.5

require (
	github.com/alecthomas/kong v0.8.1
	github.com/oapi-codegen/runtime v1.1.1
	github.com/simukti/sqldb-logger v0.0.0-20230108154142-840120f68bea
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/gammazero/deque v0.2.0 // indirect
	github.com/go-gorp/gorp/v3 v3.1.0 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	golang.org/x/sys v0.15.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	modernc.org/libc v1.37.6 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/memory v1.7.2 // indirect
	modernc.org/sqlite v1.28.0 // indirect
)

require (
	github.com/DATA-DOG/go-sqlmock v1.5.2
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/benbjohnson/immutable v0.4.3
	github.com/deepmap/oapi-codegen v1.16.2
	github.com/gammazero/workerpool v1.1.3
	github.com/glebarez/go-sqlite v1.22.0
	github.com/google/uuid v1.5.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/rossmerr/bitvector v0.0.0-20230307153838-e3c616ef0f0b
	github.com/rubenv/sql-migrate v1.6.1
	github.com/samber/lo v1.39.0
	github.com/stretchr/testify v1.9.0
	github.com/vargspjut/wlog v1.0.11
	golang.org/x/exp v0.0.0-20240222234643-814bf88cf225 // indirect
)
