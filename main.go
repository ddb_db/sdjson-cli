package main

import (
	"os"

	"github.com/vargspjut/wlog"

	"github.com/alecthomas/kong"
	"gitlab.com/ddb_db/sdjson-cli/internal/appruntime"
	"gitlab.com/ddb_db/sdjson-cli/internal/commands"
	"gitlab.com/ddb_db/sdjson-cli/internal/sdjson"
	"gitlab.com/ddb_db/sdjson-cli/internal/security"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/cache"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/datastore"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/fntimer"
	"gitlab.com/ddb_db/sdjson-cli/internal/services/settings"
)

var AppVersion = "dev-unknown"
var BuildDate = ""

func main() {
	cli := new(commands.CLI)
	ctx := kong.Parse(cli)
	configLog(cli.Verbose - cli.Quiet)
	fntimer.EnabledCategories = cli.MeasureFunctionRuntime
	appInfo := appruntime.AppInfo{Version: AppVersion, BuildDate: BuildDate}
	appruntime.AppDetails = appInfo
	ctx.Bind(appInfo)
	if ctx.Args[0] != "version" {
		if cli.DebugRestCalls || cli.DebugDbCalls {
			mkLogDir()
		}
		datastore.InitLogger(cli.DebugDbCalls)
		settingsSvc := settings.New(datastore.New(cli.DBFile, !cli.NoCreateDB, cli.DebugDbCalls))
		cac := cache.New(datastore.Get())
		creds := security.NewSDJSONCreds(cli.Login, cli.Password, settingsSvc)
		api := sdjson.Login(cli.Server.String(), creds.Login(), creds.Password(), cli.DebugRestCalls)
		ctx.BindTo(api, (*sdjson.ClientWithResponsesInterface)(nil))
		ctx.BindTo(cac, (*cache.Cache)(nil))
		ctx.BindTo(wlog.DefaultLogger(), (*wlog.Logger)(nil))
		ctx.Bind(datastore.Get())
	}
	ctx.FatalIfErrorf(ctx.Run())
}

func mkLogDir() {
	name, err := os.MkdirTemp("", "sdjson-cli.*")
	if err != nil {
		panic(err)
	}
	appruntime.LogDir = name
	wlog.Debugf("LOG DIR: %s", name)
}

func configLog(lvl int) {
	l := wlog.Nfo
	if lvl > 0 {
		l = wlog.Dbg
	} else if lvl == -1 {
		l = wlog.Wrn
	} else if lvl == -2 {
		l = wlog.Err
	} else if lvl < -2 {
		l = wlog.Ftl
	}
	wlog.DefaultLogger().SetLogLevel(l)
}
